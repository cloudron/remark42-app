#!/bin/bash

set -eu

mkdir -p /run/remark42 /app/data/var


if [[ ! -f /app/data/env ]]; then
    SECRET=$(openssl rand -base64 32)

    cat > /app/data/env <<EOT
SECRET="${SECRET}"

# comma-separated list of site IDs
SITE=remark42

AUTH_EMAIL_ENABLE=true
EOT

fi

echo "==> Reading /app/data/env"
export $(grep -v '^#' /app/data/env | xargs)

export REMARK_URL=${CLOUDRON_APP_ORIGIN}

export SMTP_HOST=${CLOUDRON_MAIL_SMTP_SERVER}
export SMTP_PORT=${CLOUDRON_MAIL_SMTPS_PORT}
export SMTP_TLS=true
export SMTP_STARTTLS=false
export SMTP_LOGIN_AUTH=false
export SMTP_INSECURE_SKIP_VERIFY=true
export SMTP_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}
export SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}
export AUTH_EMAIL_FROM=${CLOUDRON_MAIL_FROM}
export NOTIFY_EMAIL_FROM=${CLOUDRON_MAIL_FROM}

echo "==> Preparing environment"
cp -R /app/pkg/remark42/frontend/apps/remark42/public /run/remark42/

# replace {% REMARK_URL %} by content of REMARK_URL variable
find /app/code/web/ -regex '.*\.\(html\|js\|mjs\)$' -print -exec sed -i "s|{% REMARK_URL %}|${REMARK_URL}|g" {} \;

# replace 'site_id: "remark"' by the first SITE from the comma-separated list of IDs, if present
if [ -n "${SITE}" ]; then
    sep=','
    case ${SITE} in
        *"$sep"*)
            export single_site_id=${SITE%%"$sep"*}
            echo "multiple site IDs passed in SITE (\"${SITE}\"): using \"${single_site_id}\" in frontend site_id"
            ;;
        *)
            echo "using non-standard frontend site_id from SITE variable (\"${SITE}\") instead of \"remark\""
            export single_site_id=$SITE
            ;;
    esac
#    echo "single_site_id: ${single_site_id}"
    sed -i "s|site_id:\"[^\"]*\"|site_id:\"${single_site_id}\"|g" /app/code/web/*.html
fi

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data /run/remark42

cp /app/pkg/remark42-nginx.conf /run/remark42-nginx.conf

echo "==> Starting Remark42"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Remark42
