#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    readlinePromises = require('readline/promises'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const rl = readlinePromises.createInterface({ input: process.stdin, output: process.stdout });

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_MESSAGE = 'Hello Test!';
    const USERNAME = 'tester';
    const EMAIL = process.env.EMAIL ? process.env.EMAIL : 'test@cloudron.io';

    const TEST_TIMEOUT = 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;
    let host_os;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function getOS() {
        if (typeof(host_os) == 'undefined' || host_os == null)
            host_os = String(await execSync('uname -s')).trim();
        return host_os;
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function switchToIframe(xpath) {
        const frame = browser.findElement(By.xpath(xpath));
        await browser.switchTo().frame(frame);
    }

    async function switchToRemark42Frame() {
        await switchToIframe('//div[@id="remark42"]/iframe');
    }

    async function switchToCommentsFrame() {
        await switchToIframe('//iframe[contains(@class, "widget__comments-frame")]');
    }

    async function switchToMainContext() {
        await browser.switchTo().defaultContent();;
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/web`);
        await browser.sleep(3000);
        await switchToRemark42Frame();
        await waitForElement(By.xpath('//button[contains(@class, "auth-button") and contains(text(), "Sign In")]'));

        await browser.findElement(By.xpath('//button[contains(@class, "auth-button") and contains(text(), "Sign In")]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(USERNAME);
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(EMAIL);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[contains(@class, "auth-submit") and contains(text(), "Submit")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//textarea[@name="token"]'));


        let say_cmd = await getOS() == "Darwin" ? "say" : "spd-say";
        execSync(`${say_cmd} "Paste token from email in ${EMAIL} mailbox"`);
        await rl.question(`Pasted token from email in ${EMAIL} mailbox? `);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[contains(@class, "auth-submit") and contains(text(), "Submit")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath(`//button[contains(@class, "user-profile-button") and contains(text(), "${USERNAME}")]`));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/web`);
        await browser.sleep(2000);
        await switchToRemark42Frame();

        await browser.wait(until.elementLocated(By.xpath(`//div[contains(@class, "user-logout-button")]/button[contains(@title, "Sign Out")]`)), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//div[contains(@class, "user-logout-button")]/button[contains(@title, "Sign Out")]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//button[contains(@class, "auth-button") and contains(text(), "Sign In")]'));
    }

    async function sendMessage() {
        await browser.get(`https://${app.fqdn}/web`);
        await browser.sleep(2000);
        await switchToRemark42Frame();

        await waitForElement(By.xpath('//textarea[@placeholder="Your comment here"]'));

        await browser.findElement(By.xpath('//textarea[@placeholder="Your comment here"]')).sendKeys(TEST_MESSAGE);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[contains(text(), "Send")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath(`//*[text()="${TEST_MESSAGE}"]`));
    }

    async function checkMessage() {
        await browser.get(`https://${app.fqdn}/web`);
        await browser.sleep(2000);
        await switchToCommentsFrame();
        await waitForElement(By.xpath(`//*[contains(., "${TEST_MESSAGE}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can send message', sendMessage);
    it('can check message', checkMessage);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });
    it('can login', login);
    it('check message', checkMessage);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');

        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

//  it('can login', login);
    it('check message', checkMessage);
//  it('can logout', logout.bind(null, username));

    it('move to different location', async function () {
        await browser.get('about:blank');

        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('check message', checkMessage);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

/*
    // test update
    it('can install previous version from appstore', function () { execSync(`cloudron install --appstore-id com.remark42.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', login);
    it('can send message', sendMessage);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);

    it('check message', checkMessage);
    it('can send message', sendMessage);
    it('check message', checkMessage);

    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
*/
});
