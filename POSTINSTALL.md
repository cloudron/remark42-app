Admins/moderators should be defined in `/app/data/env` as a list of user IDs, e.g.

```
ADMIN_SHARED_ID=github_ef0f706a79cc24b17bbbb374cd234a691a034128,github_dae9983158e9e5e127ef2b87a411ef13c891e9e5
```

To get a user ID just log in and click on your username or any other user you want to promote to admins. It will expand login info and show the full user ID.


Don't forget to restart the app to get settings applied.


See documentation for more details:
https://remark42.com/docs/configuration/parameters/
https://remark42.com/docs/manuals/admin-interface/
