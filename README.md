# cloudron-stirling-pdf
Package for deloyment of stirling-pdf on Cloudron

## THIS IS ONLY A PROJECT TO MAKE IT POSSIBLE TO DEPLOY STIRLING-PDF ON CLOUDRON 

To deploy it manually on Cloudron follow this instruction:

Build the docker Image:
~~~
docker build -t APPNAME .
~~~

Tag the docker Image:
~~~
docker tag APPNAME:latest URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Push Image to registry:
~~~
docker push URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Install image on your Cloudron Instance:
~~~
cloudron install --image URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

## License
This project is under the [GNU GPLv3](LICENSE).

