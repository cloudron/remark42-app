FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg/remark42 /app/pkg/nginx
WORKDIR /app/code

# install NODE v17
ARG NODE_VERSION=17.9.1
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH=/usr/local/node-${NODE_VERSION}/bin:$PATH

# install Go lang v1.21.4
ARG GO_VERSION=1.21.4
RUN curl -L https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz | tar -xz -f - -C /usr/local
ENV PATH=$PATH:/usr/local/go/bin

# install Remark app
ARG VERSION=1.13.0
RUN curl -L https://github.com/umputun/remark42/archive/refs/tags/v${VERSION}.tar.gz | tar -xz --strip-components 1 -f - -C /app/pkg/remark42
# get TLS InsecureSkipVerify option commit (needs to be replaced once released)
#RUN curl -L https://codeload.github.com/umputun/remark42/tar.gz/94893b77dc1a0f4e0ba6408640f7a8cbdbf89f25 | tar -xz --strip-components 1 -f - -C /app/pkg/remark42

# frontend build
WORKDIR /app/pkg/remark42/frontend/apps/remark42/

ARG PNPM_VERSION=7.33.6
RUN npm install --global pnpm@${PNPM_VERSION} && \
    pnpm i --frozen-lockfile && \
    pnpm build

# backend build
WORKDIR /app/pkg/remark42/backend/

# to embed the frontend files statically into Remark42 binary
RUN cp -R /app/pkg/remark42/frontend/apps/remark42/public/. /app/pkg/remark42/backend/app/cmd/web/

RUN find /app/pkg/remark42/backend/app/cmd/web/ -regex '.*\.\(html\|js\|mjs\)$' -print -exec sed -i "s|{% REMARK_URL %}|http://127.0.0.1:8080|g" {} \;

# run locally to get app version
#
# git checkout v${VERSION}
# ref=$(git describe --tags --exact-match 2> /dev/null || git symbolic-ref -q --short HEAD)
# version="${ref}"-$(git log -1 --format=%h)-$(date +%Y%m%dT%H:%M:%S)
# https://github.com/umputun/baseimage/blob/master/build.go/version.sh
ARG REF=v${VERSION}
ARG COMMIT=661f042
ARG REVISION_DATE=20240516T16:16:21
ARG REMARK42_REVISION="${REF}-${COMMIT}-${REVISION_DATE}"

RUN go build -o remark42 -ldflags "-X main.revision=${REMARK42_REVISION} -s -w" ./app

WORKDIR /app/code
RUN cp /app/pkg/remark42/backend/remark42 /app/code && \
    ln -sf /run/remark42/public /app/code/web && \
    ln -sf /app/data/var /app/code/var

# fix up scripts path
RUN scripts=("backup" "import" "restore") && \
    for script in ${scripts[@]}; do sed -e "s/\/srv/\/app\/code/g" /app/pkg/remark42/backend/scripts/${script}.sh > /app/code/${script} && chmod a+x /app/code/${script}; done

# add nginx config
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -sf /run/remark42-nginx.conf /etc/nginx/sites-enabled/remark42.conf
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
COPY nginx/remark42.conf /app/pkg/remark42-nginx.conf

# add placeholder page assets
COPY www /app/pkg/www
COPY logo.png /app/pkg/www/logo.png

# supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
